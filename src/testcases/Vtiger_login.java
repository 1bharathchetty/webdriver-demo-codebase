package testcases;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.graylocus.util.SimpleReportFactory;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Vtiger_login {
	
private static ExtentReports reporter = SimpleReportFactory.getReporter();

	public static WebDriver browser;

	@BeforeSuite
	public static void launchURL(){
		System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
		browser = new ChromeDriver();
		browser.manage().window().maximize();
		browser.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}
	
	@Test
	public void loadPage(){
		
		ExtentTest testReporter = reporter.startTest("loadPage", "This is a simple simpleTest001");
		testReporter.log(LogStatus.INFO, "Starting test loadPage");
		
		browser.get("http://www.binaryzombies.com/vtiger");
		
		testReporter.log(LogStatus.INFO, "launching url");
		
		reporter.endTest(testReporter);
		reporter.flush();
	
	}
	
	@Test
	public void loginWith() {

		ExtentTest testReporter = reporter.startTest("Login Page","Login with Username and password");
				
		browser.findElement(By.xpath("//input[@name='username']")).sendKeys("crmuser");
		testReporter.log(LogStatus.PASS, "username Enterd");
		browser.findElement(By.xpath("//input[@name='password']")).sendKeys("crmuser");
		testReporter.log(LogStatus.PASS, "passwors entered");
		browser.findElement(By.xpath("//button[text()='Sign in']")).click();
		testReporter.log(LogStatus.PASS, "click signIn button");
		reporter.endTest(testReporter);
		reporter.flush();
	
	}

}
