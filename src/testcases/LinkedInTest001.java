package testcases;


import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LinkedInTest001 {

	@Test
	public void registrationTest() throws InterruptedException {
		
	
		//launch a browser
		//chrome driver
		System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
		WebDriver googleChrome = new ChromeDriver();
		
		//maximize the current window
		googleChrome.manage().window().maximize();
		
		// wait for 15 seconds before throwing error in finding elements
		googleChrome.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		// wait for 30 seconds when there is a page being loaded
		googleChrome.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);


		
		//go to the linkedin home page
		googleChrome.get("https://www.linkedin.com");
		
		
		// fill first name
		WebElement firstNameTextbox = googleChrome.findElement(By.xpath("//input[@name ='firstName']"));
		firstNameTextbox.sendKeys("Bharath");
		
		
		// fill last name
		WebElement lastNameTextbox = googleChrome.findElement(By.xpath("//input[@name ='lastName']"));
		lastNameTextbox.sendKeys("Chetty");
		
		// fill email id
		WebElement userEmailTextbox = googleChrome.findElement(By.xpath("//input[@id ='reg-email']"));
		userEmailTextbox.sendKeys("atestology@gmail.com");
		
		// fill password
		WebElement passwordTextbox = googleChrome.findElement(By.xpath("//input[@id ='reg-password']"));
		passwordTextbox.sendKeys("$1billion");
		
		// submit by clicking on JOin now
		WebElement joinNowButton = googleChrome.findElement(By.xpath("//input[@value ='Join now']"));
		joinNowButton.click();
		
		Thread.sleep(5000);
		
		// verify the presence of Trying to Sign In text
		WebElement tryingToSignInText  = googleChrome.findElement(By.xpath("//h3[text()= 'Trying to sign in?']"));
		System.out.println(tryingToSignInText.isDisplayed());
		assertTrue(tryingToSignInText.isDisplayed());
		
		// verify the expected response is displayed
		//WebElement existingUserErrorMessage = googleChrome.findElement(By.xpath("//*[text() ='Someone's already using that email. If that’s you, enter your Email and password here to sign in.']"));
		WebElement existingUserErrorMessage = googleChrome.findElement(By.xpath("//*[contains(text(), 'already using that email')]"));
		System.out.println(existingUserErrorMessage.isDisplayed());
		assertTrue(existingUserErrorMessage.isDisplayed());
	
/*		if(existingUserErrorMessage.isDisplayed()){
			System.out.println("test passed");
		}else{
			System.err.println("test failed");

		}*/
		
	}
	
	@Test
	public void demoTest(){
		assertTrue(true);

	}

}
