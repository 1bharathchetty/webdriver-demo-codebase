package com.graylocus.selenium;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class RunningJavascript {

	//@Test
		public void runningJavascriptOnPageTests(){
			FirefoxDriver ff = new FirefoxDriver();

			ff.get("http://encreo.com/crm");
			ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
			ff.manage().window().maximize();		
			
			JavascriptExecutor jsRunner =  (JavascriptExecutor)ff;
			 Object returnedObject = jsRunner.executeScript("return document.title;");
			 System.out.println("Title of the page is \""+returnedObject.toString() + "\"");
			 
			 jsRunner.executeScript("document.getElementById('username').value='bharathc';");
			 jsRunner.executeScript("document.getElementById('password').value='somepassword';");
			 jsRunner.executeScript("document.getElementsByTagName('button')[0].click();");

			ff.quit();
		}
		
		//@Test
		public void runningJavascriptOnElementsTests() throws InterruptedException{
			FirefoxDriver ff = new FirefoxDriver();

			ff.get("http://encreo.com/crm");
			ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
			ff.manage().window().maximize();		
			
			JavascriptExecutor jsRunner =  (JavascriptExecutor)ff;
			 Object returnedObject = jsRunner.executeScript("return document.title;");
			 System.out.println("Title of the page is \""+returnedObject.toString() + "\"");
			 
			 WebElement usernameTextbox = ff.findElement(By.id("username"));
			 jsRunner.executeScript("arguments[0].value='crmuser';", usernameTextbox);
			 WebElement passwordTextbox = ff.findElement(By.id("password"));
			 jsRunner.executeScript("arguments[0].value='crmuser';", passwordTextbox);
			 WebElement signinButton = ff.findElement(By.xpath("//button[.='Sign in']"));
			 jsRunner.executeScript("arguments[0].click();", signinButton );
			 
			 Thread.sleep(5000);
			 WebElement signOutLink = ff.findElement(By.xpath("//a[.='Sign Out']"));
			 //signOutLink.click(); // element not visible exception
			 jsRunner.executeScript("arguments[0].click();", signOutLink ); // running javascript can work on elements that are not visible.
			 // for invisible elements, webdriver throws element not visible and cannot be interacted with exception.
			 Thread.sleep(5000);

			// jsRunner.executeScript("arguments[0].dblclick();", signOutLink ); // running javascript to perform a double click on an element (The element should first support it as shown below).
			 //<p ondblclick="myFunction()">Double-click me</p>


			 //jsRunner.executeScript("arguments[0].blur();", someElementWithBlurEvent );  // to trigger a blur event => moving the focus out of the element
			 ff.quit();
		}
		
		@Test
		public void waitingForPageToLoadUsingJStest(){
		  //	document.readyState;
			//"complete"
			FirefoxDriver ff = new FirefoxDriver();

			ff.get("http://encreo.com/crm");
			ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
			ff.manage().window().maximize();		
			
			JavascriptExecutor jsRunner =  (JavascriptExecutor)ff;
			 Object returnedObject = jsRunner.executeScript("return document.title;");
			 System.out.println("Title of the page is \""+returnedObject.toString() + "\"");
			 
			 WebElement usernameTextbox = ff.findElement(By.id("username"));
			 jsRunner.executeScript("arguments[0].value='crmuser';", usernameTextbox);
			 WebElement passwordTextbox = ff.findElement(By.id("password"));
			 jsRunner.executeScript("arguments[0].value='crmuser';", passwordTextbox);
			 WebElement signinButton = ff.findElement(By.xpath("//button[.='Sign in']"));
			 jsRunner.executeScript("arguments[0].click();", signinButton );
			 
			 Object pageStateObject = jsRunner.executeScript("return document.readyState;");
			 System.out.println("Page State of the document is \""+pageStateObject.toString() + "\"" );
			 
			 int counter=0;
			 while(pageStateObject.toString().equalsIgnoreCase("complete")==false){
				 System.out.println("The current state is "+ pageStateObject.toString());
				 System.out.println("ready state is not returned complete... hence waiting...");
				 try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 pageStateObject= jsRunner.executeScript("return document.readyState;");
				 counter++;
			 }
			 
			 System.out.println("Out of waiting loop after \""+ counter + "\" iterations for page load to complete");
			 System.out.println("The current state is "+ pageStateObject.toString());

			
			
		}
		
		
		
	}