package com.graylocus.selenium;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SwitchToWindowUsingAttribute {

	FirefoxDriver ff ;


	@Test
	public void switchToWindowUsingTitleTest1() throws Exception{
		// the two tests in this class are to show only the difference between publicly declared ff and locally declared ff
		//in this case, we are using locally declared one
		FirefoxDriver ff = new FirefoxDriver();

		ff.get("http://openboxes.com/demo.html");
		ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		ff.manage().window().maximize();
		
		ff.findElement(By.linkText("Login with an existing account")).click();
		ff.findElement(By.linkText("Signup to access demo")).click();
		
		String windowTitle1 = "OpenBoxes | About";
		String windowTitle2 = "Login";
		String windowTitle3 = "Create a new account";
		switchToWindowByTitle(ff, windowTitle2);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		
		ff.findElement(By.id("username")).sendKeys("scmuser");
		ff.findElement(By.id("password")).sendKeys("scmuser");
		ff.findElement(By.id("loginButton")).click();
		
		switchToWindowByTitle(ff, windowTitle3);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		ff.findElement(By.id("email")).sendKeys("no email validation?");
		
		switchToWindowByTitle(ff, windowTitle1);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		ff.findElement(By.linkText("Demo #2")).click();
			
		// calling a method close all windows...
		//closeAllWindows(ff);
		forceQuitCurrentSession(ff);
	}
	

	
	
	private void forceQuitCurrentSession(WebDriver ff) {
		ff.quit();
	}




	@Test
	public void switchToWindowUsingTitleTest2() throws Exception{
		ff= new FirefoxDriver();
		// the two tests in this class are to show only the difference between publicly declared ff and locally declared ff
		//in this case, we are using publicly declared one
		ff.get("http://openboxes.com/demo.html");
		ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		ff.manage().window().maximize();
		
		ff.findElement(By.linkText("Login with an existing account")).click();
		ff.findElement(By.linkText("Signup to access demo")).click();
		
		String windowTitle1 = "OpenBoxes | About";
		String windowTitle2 = "Login";
		String windowTitle3 = "Create a new account";
		switchToWindowByTitle(windowTitle2);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		
		ff.findElement(By.id("username")).sendKeys("scmuser");
		ff.findElement(By.id("password")).sendKeys("scmuser");
		ff.findElement(By.id("loginButton")).click();
		
		switchToWindowByTitle(windowTitle3);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		ff.findElement(By.id("email")).sendKeys("no email validation?");
		
		switchToWindowByTitle(windowTitle1);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		ff.findElement(By.linkText("Demo #2")).click();
		
		closeAllWindows(ff);
	}

	private void switchToWindowByTitle(WebDriver driverInstance , String expectedWindowTitle) {
		Set<String> allWindowHandles = driverInstance.getWindowHandles();
		for(String windowHandle : allWindowHandles){
			driverInstance.switchTo().window(windowHandle);
			String currentWindowTitle = driverInstance.getTitle();
			System.out.println("The focus is switched to a window with title ::"+ currentWindowTitle);
			if(currentWindowTitle.equals(expectedWindowTitle)){
				System.out.println("Title matched");
				break;
			}
		}
		
	}
	
	private void switchToWindowByTitle( String expectedWindowTitle) {
		Set<String> allWindowHandles = ff.getWindowHandles();
		for(String windowHandle : allWindowHandles){
			ff.switchTo().window(windowHandle);
			String currentWindowTitle = ff.getTitle();
			System.out.println("The focus is switched to a window with title ::"+ currentWindowTitle);
			if(currentWindowTitle.equals(expectedWindowTitle)){
				System.out.println("Title matched");
				break;
			}
		}
	}
	
	private void closeAllWindows(WebDriver driverInstance){
		Set<String> allWindowHandles = driverInstance.getWindowHandles();
		for(String windowHandle : allWindowHandles){
			driverInstance.switchTo().window(windowHandle);
			driverInstance.close(); // closes the current window whichever is in focus
		}
		
	}
	

}

