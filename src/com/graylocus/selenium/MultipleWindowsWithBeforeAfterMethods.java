package com.graylocus.selenium;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MultipleWindowsWithBeforeAfterMethods {

FirefoxDriver ff;
	
	@Before
	public void startBrowser(){
		ff = new FirefoxDriver();
		ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		ff.manage().window().maximize();

	}
	
	@Test
	public void yourCodeInATest() throws InterruptedException{
		System.out.println("Executing test one ");

		ff.get("http://openboxes.com/demo.html");
		ff.findElement(By.linkText("Login with an existing account")).click();
		ff.findElement(By.linkText("Signup to access demo")).click();
		
		String windowTitle1 = "OpenBoxes | About";
		String windowTitle2 = "Login";
		String windowTitle3 = "Create a new account";
		switchToWindowByTitle(windowTitle2);
		Thread.sleep(5000);  // wait for 5 seconds , only for demo. Not to be used in actual code
		
		ff.findElement(By.id("username")).sendKeys("scmuser");
		ff.findElement(By.id("password")).sendKeys("scmuser");
		ff.findElement(By.id("loginButton")).click();
	}
	
	
	@Test
	public void yourGoogleTest(){
		System.out.println("Executing test two ");
		ff.get("http://google.com");
		ff.findElement(By.name("q")).sendKeys("Junit github examples");

	}
	
	
	@Test
	public void anotherTest(){
		System.out.println("Executing test three ");
		ff.get("http://fb.com");
		ff.findElement(By.name("q")).sendKeys("Junit github examples");

	}
	@After
	public void closeSession(){
		ff.quit();
	}
	
	private void switchToWindowByTitle( String expectedWindowTitle) {
		Set<String> allWindowHandles = ff.getWindowHandles();
		for(String windowHandle : allWindowHandles){
			ff.switchTo().window(windowHandle);
			String currentWindowTitle = ff.getTitle();
			System.out.println("The focus is switched to a window with title ::"+ currentWindowTitle);
			if(currentWindowTitle.equals(expectedWindowTitle)){
				System.out.println("Title matched");
				break;
			}
		}
	}

}

