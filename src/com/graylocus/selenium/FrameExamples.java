package com.graylocus.selenium;

import java.util.concurrent.TimeUnit;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FrameExamples {
	//@Test
		public void shipMyCargoTest(){
			FirefoxDriver ff = new FirefoxDriver();

			ff.get("http://shipmycargo.com/");
			ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
			ff.manage().window().maximize();		
			
			ff.switchTo().frame("SHIPMYCARGO.COM");
			System.out.println("switched to SHIPMYCARGO.COM");
			
			ff.switchTo().frame("BERKLAY.COM");
			System.out.println("switched to BERKLAY.COM");
			
			WebElement quoteRequestLink = ff.findElement(By.xpath("//a[.='Quote Request']"));
			quoteRequestLink.click();
			
			WebElement quoteFirstNameTextbox = ff.findElement(By.name("First Name"));
			quoteFirstNameTextbox.sendKeys("bharath");
			
			WebElement quoteLastNameTextbox = ff.findElement(By.name("Last Name"));
			quoteLastNameTextbox.sendKeys("chetty");
		}
		
		
		@Test
		public void framesUsingLocalHTMLTest(){
			System.setProperty("webdriver.chrome.driver", "D:\\bc\\ws\\WD\\drivers\\chromedriver.exe");
			ChromeDriver gc = new ChromeDriver();
			gc.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			gc.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
			gc.manage().window().maximize();		
			
			gc.get("file:///D:/bc/ws/WD/sampleHTML/Frames.html");
			
			
			gc.switchTo().frame("frameTwo");
			gc.findElement(By.xpath("//a[.='go to encreo from here']")).click();
			//gc.findElement(By.name("sometext")).sendKeys("I am in frame two ");
			
			gc.switchTo().defaultContent(); // this is to switch the control back the highest parent in the page , which is the default content 
			
			gc.switchTo().frame("frameOne"); 
			
			gc.findElement(By.name("textone")).sendKeys("I AM IN FRAME ONE");
			
			gc.switchTo().defaultContent();// this is to switch the control back the highest parent in the page , which is the default content 
			
			gc.switchTo().frame("frameThree");
			
			gc.findElement(By.name("frame3Textbox")).sendKeys("I AM IN FRAME Three");
			
		}

	}