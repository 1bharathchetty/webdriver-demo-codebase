package com.graylocus.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class CrossBrowserExecutionWithoutJunit {
	public static void main(String[] args) {
		usingFirefox();
		usingIE();
		usingGoogleChrome();

	}

	public static void usingFirefox(){
		System.out.println("running firefox browser");
		WebDriver ff = new FirefoxDriver();
		ff.get("http://google.com");
	}

	
	public static void usingIE(){
		System.out.println("running IE browser");
		System.setProperty("webdriver.ie.driver", "D:\\bc\\ws\\WD\\drivers\\IEDriverServer64.exe");
		WebDriver ie = new InternetExplorerDriver();
		ie.get("http://graylocus.com");
	}
	
	public static void usingGoogleChrome(){
		System.out.println("running Google Chrome browser");
		System.setProperty("webdriver.chrome.driver", "D:\\bc\\ws\\WD\\drivers\\chromedriver.exe");
		WebDriver gc = new ChromeDriver();
		gc.get("http://encreo.com");

	}
	
}
