package com.graylocus.selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;


public class UsingDropdowns {

	private static String xp_SelectYear ="//select[@id='ymm_year']";
	private static String xp_SelectMake ="//select[@id='ymm_make']";
	private static String xp_SelectModel ="//select[@id='ymm_model']";
	private static String xp_SelectSubModel ="//select[@id='ymm_submodel']";
	private static String xp_SearchButton ="//input[@value='Search']";
	
	private static String expectedPageTitle= "JC Whitney";
	private static String applicationURL ="http://www.jcw.com/";
	private static String selectYear ="2015";
	private static String selectMake= "Audi";
	private static String selectModel = "Q7";
	private static String selectSubModel ="Sport";
	
	
	private static WebDriver driver;
	public static void main(String[] args) {
		
		startFirefoxWithWindowMaximized();
		goToURL(applicationURL);
		verifyThePageHasTitle(expectedPageTitle);
		performSearchUsingYMMSelector();
		String searchResultsPageTitle = driver.getTitle();
		System.out.println("The search results page has title \"" + searchResultsPageTitle + "\"");
		verifyPageHasHeaderTextAsExpected();
		closeBrowser();
	
	}

	private static void verifyPageHasHeaderTextAsExpected() {
		
		// using the get text approach
		String expectedHeaderText = selectYear + " "+ selectMake + " "+ selectModel + " "+ selectSubModel + " Parts & Accessories";
		System.out.println("Expected header text \""+ expectedHeaderText + "\"");
		String xp_headerTextElement = "//h1";  
		WebElement headerTextElement = driver.findElement(By.xpath(xp_headerTextElement));
		String actualHeaderText = headerTextElement.getText().trim();
		if(actualHeaderText.equals(expectedHeaderText)){
			System.out.println("Header Text matches for the inputs");
		}else{
			System.err.println("Header text doesn't match ");
			System.out.println("Expected::"+ expectedHeaderText);
			System.out.println("Actual  ::"+ actualHeaderText);
		}
		
		// using dynamic locator approach
		String dynamicLocatorForHeaderText = "//strong[contains(text(), '"+ expectedHeaderText +"')]";
		if( driver.findElements(By.xpath(dynamicLocatorForHeaderText)).size()==1){
			System.out.println("Header text is found with expected data");
		}else{
			System.out.println("Header text is NOT found with expected data");

		}
		
	}

	private static void closeBrowser() {
		driver.close();
	}

	private static void performSearchUsingYMMSelector() {
		WebElement yearElement = driver.findElement(By.xpath(xp_SelectYear));
		Select yearList = new Select(yearElement);
		yearList.selectByVisibleText(selectYear);
		//yearList.selectByValue("2020");
		
		Select makeList = new Select(driver.findElement(By.xpath(xp_SelectMake)));
		makeList.selectByVisibleText(selectMake);
		//makeList.selectByValue("7:Audi");
		
		Select modelList = new Select(driver.findElement(By.xpath(xp_SelectModel)));
		modelList.selectByVisibleText(selectModel);
		//modelList.selectByValue("17945:Q7");
		
		Select submodelList = new Select(driver.findElement(By.xpath(xp_SelectSubModel)));
		submodelList.selectByVisibleText(selectSubModel);
		List<WebElement> submodelOptions = submodelList.getOptions();
		int optionsCount = submodelOptions.size();
		//submodelList.selectByIndex(optionsCount-1);
		
		WebElement searchButton = driver.findElement(By.xpath(xp_SearchButton));
		searchButton.click();
		
		
		
	}

	private static void startFirefoxWithWindowMaximized() {
		driver= new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	}
	
	private static void goToURL(String url) {
		driver.get(url);
		String currentURL = driver.getCurrentUrl();
		System.out.println("Current URL:"+ currentURL);
	}
	
	private static void verifyThePageHasTitle(String expectedPageTitle) {
		String actualTitle = driver.getTitle();
		if(actualTitle.equals("JC Whitney Auto Parts & Auto Accessories - Car, Truck, Jeep, Motorcycle, VW, RV & ATV - Aftermarket Parts & Accessories")){  // exact match
			System.out.println("the page has the title \""+actualTitle + "\" as expected \""+ expectedPageTitle + "\"" );
		}else{
			System.out.println("Fail, page title doesn't match: actual title \""+actualTitle + "\" & expected \""+ expectedPageTitle + "\"" );
		}
		
		if(actualTitle.contains(expectedPageTitle)){  // partial match
			System.out.println("the page has the title \""+actualTitle + "\" as expected \""+ expectedPageTitle + "\"" );
		}else{
			System.out.println("Fail, page title doesn't match: actual title \""+actualTitle + "\" & expected \""+ expectedPageTitle + "\"" );
		}
	}

}
