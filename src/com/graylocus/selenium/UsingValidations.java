package com.graylocus.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class UsingValidations {
	int count;
	String name = "bharath";
	static WebDriver driver;
	private static String leadFirstName ="Suyodhana";
	private static String leadLastName ="Kuruvamshi";
	
	public static void main(String[] args) {
		
		System.out.println("My First proper Test with validations");
		
		launchFirefox();
		goToVtigerLoginPage();
		logintoVtiger();
		goToLeadsPage();
		goToCreateLeadPage();
		createLead();
		verifySavedLeadInformationAndFetchLeadIdGenerated();
		signout();
		closeBrowser();
	
	}


	private static void launchFirefox() {
		System.out.println("launchFirefox");
		driver = new FirefoxDriver();  // This line helps you launch Firefox browser, usually minimized
		driver.manage().window().maximize(); // Helps you maximize the browser window
/*		driver = new ChromeDriver();
		driver = new SafariDriver();
		driver = new InternetExplorerDriver();*/
		
		
	}
	
	private static void goToVtigerLoginPage() {
		System.out.println("goToVtigerLoginPage");
		driver.get("http://encreo.com/crm"); // use this to launch your URL

	}
	
	private static void logintoVtiger() {
		System.out.println("logintoVtiger");
		WebElement usernameTextbox = driver.findElement(By.id("username"));
		//WebElement usernameTextbox = driver.findElement(By.xpath("//input[@id='username']"));
		usernameTextbox.sendKeys("crmuser");
		WebElement passwordTextbox = driver.findElement(By.name("password"));
		//WebElement passwordTextbox = driver.findElement(By.xpath("//input[@name='password']"));
		passwordTextbox.sendKeys("crmuser");
		WebElement signinButton = driver.findElement(By.tagName("button"));
		//WebElement signinButton = driver.findElement(By.xpath("//button"));
		signinButton.click();
	
	}


	
	private static void goToLeadsPage(){
		System.out.println("goToLeadsPage");
		WebElement leadsTab = driver.findElement(By.xpath("//strong[text()='Leads']"));
		leadsTab.click();

	}
	
	private static void goToCreateLeadPage(){
		System.out.println("goToCreateLeadPage");
		WebElement createLeadButton = driver.findElement(By.xpath("//strong[text()='Add Lead']"));
		createLeadButton.click();

	}
	
	
	private static void createLead(){
		System.out.println("createLead");
		WebElement firstNameTextbox = driver.findElement(By.xpath("//input[@name='firstname']"));
		firstNameTextbox.sendKeys(leadFirstName);
		WebElement lastNameTextbox = driver.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']"));
		lastNameTextbox.sendKeys(leadLastName);
		//WebElement firstSaveButton = driver.findElement(By.xpath("//strong[text()='Save']"));
		WebElement secondSaveButton = driver.findElement(By.xpath("(//strong[text()='Save'])[2]"));
		secondSaveButton.click();

	}
	
	
	private static void verifySavedLeadInformationAndFetchLeadIdGenerated() {
		WebElement showFullDetailsButton  = driver.findElement(By.xpath("//button[.='Show Full Details']"));
		showFullDetailsButton.click();

		// using xpath that doesn't use testdata
		WebElement headerTextWithLeadFullName = driver.findElement(By.xpath("//h4[@class='recordLabel pushDown']"));
		String actualFullNameDisplayed = headerTextWithLeadFullName.getText().trim(); // trim basically removes trailing spaces
		System.out.println("Actual full name  displayed \""+ actualFullNameDisplayed + "\"");

		
		String expectedFullName = leadFirstName +" " +  leadLastName;
		System.out.println("Expected full name \""+ expectedFullName + "\"");
		
		if(actualFullNameDisplayed.equals(expectedFullName)){
			System.out.println("Lead's full name is displayed properly");
		}else{
			System.err.println("Lead's full name is not displayed properly");
			System.out.println("Actual   : "+ actualFullNameDisplayed);
			System.out.println("Expected : "+ expectedFullName);
		}
		
		//capturing the lead number that is generated
		
		try {
			Thread.sleep(3000); // not a good solution, for ajax behavior. explicit waits should be used here.
			String xp_LeadNumberGenerated = "//label[.='Lead Number']/../following-sibling::td[1]/span";
			String leadNumberGenerated = driver.findElement(By.xpath(xp_LeadNumberGenerated)).getText().trim();
			System.out.println( "The lead number generated  :: \"" +leadNumberGenerated+ "\"");	
		}catch(Exception e){
			System.err.println("Exception in fetching lead number generated . "+ e.getMessage());
		}

		
		
		//using xpath that uses testdata to create dynamic locators

		try{
			System.out.println("Using dynamic xpath constructed using testdata");
			String expectedFullNameInElement = leadFirstName +" " +  leadLastName;
			// String expectedFullname  = "Bharath" + " "+ "Chetty";
			//String xpathDynamic = //h4[contains(text(),'Bharath Chetty' )];
			String xpath_expectedHeaderTextElement = "//h4[contains(@title, '"+expectedFullNameInElement+"')]";
			
			WebElement headrTextElement = driver.findElement(By.xpath(xpath_expectedHeaderTextElement));
			if(headrTextElement.isDisplayed()){
				System.out.println("Lead's full name is displayed properly");
			}else{
				System.out.println("Nope, Lead's full name is NOT displayed properly");
			}
		}catch(Exception e){
			System.out.println("Exception in Using dynamic xpath constructed using testdata :"+ e.getMessage());
		}

		// Verifying in the table for first name and last name displayed
		// Using getText Approach
		System.out.println("Using getText Approach");
		String xp_FirstNameInTable  = "//span[@class='firstname']";
		String xp_alternateFirstNameInTable  = "//label[.='First Name']/../following-sibling::td[1]";
		String xp_LastNameInTable = "//span[@class='lastname']";
		String xp_alternatLastNameInTable  = "  1]";  // used * instead of td, cos the td has only t
		// td elements as siblings.
		
		String firstNameInTable = driver.findElement(By.xpath(xp_alternateFirstNameInTable)).getText().trim();
		if(leadFirstName.equals(firstNameInTable)){
			System.out.println("first name matches");
		}else{
			System.out.println("first name doesn't match");

		}
		
		String lastNameInTable = driver.findElement(By.xpath(xp_alternatLastNameInTable)).getText().trim();
		if(leadLastName.equals(lastNameInTable)){
			System.out.println("last name matches");
		}else{
			System.out.println("last name doesn't match");

		}
		
		// Verifying in the table for first name and last name displayed
		// Using dynamic locators approach
		System.out.println("Using dynamic locators approach");
		String xp_DynamicFirstNameInTable  = "//span[@class='firstname'][contains(text(), '" + leadFirstName+"')]";
		String xp_DynamicLastNameInTable = "//span[@class='lastname'][contains(text(), '"+ leadLastName+"')]";
		
		if(driver.findElement(By.xpath(xp_DynamicFirstNameInTable)).isDisplayed()){
			System.out.println("first name matches");
		}else{
			System.out.println("first name doesn't match");
		}
		
		if(driver.findElement(By.xpath(xp_DynamicLastNameInTable)).isDisplayed()){
			System.out.println("last  name matches");
		}else{
			System.out.println("last name doesn't match");
		}
		
	}
	
	
	
	private static void signout() {
		System.out.println("signout");
		WebElement usermenuLink = driver.findElement(By.xpath("//strong[text()='Demo']"));
		usermenuLink.click();
		WebElement signoutLink = driver.findElement(By.xpath("//a[text()='Sign Out']"));
		signoutLink.click();

	}
	
	private static void closeBrowser(){
		System.out.println("closeBrowser");
		driver.close();

	}

}

