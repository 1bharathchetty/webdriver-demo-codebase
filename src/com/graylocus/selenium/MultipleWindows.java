package com.graylocus.selenium;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;


public class MultipleWindows {
	public static void main(String[] args) {
		FirefoxDriver ff = new FirefoxDriver();
		
		ff.get("http://openboxes.com/demo.html");
		ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		ff.manage().window().maximize();
		
		ff.findElement(By.linkText("Login with an existing account")).click();
		ff.findElement(By.linkText("Signup to access demo")).click();
		
		Set<String> windowHandles = ff.getWindowHandles();
		System.out.println("Windows count ::"+  windowHandles.size());
		//ff.switchTo().
		
		int windowCounter  = 1;
		for(String winHandle : windowHandles){
			ff.switchTo().window(winHandle);
			System.out.println("The focus is shifted to window count ::"+ windowCounter);
			System.out.println("The window in focus now has title ::" + ff.getTitle());
			if(windowCounter ==2){
				break;  // Get out of for loop, if the window counter shows 2, to switch to second window
			}
			windowCounter++;
		
		}
		
		ff.findElement(By.name("username")).sendKeys("scmuser");
		ff.findElement(By.name("password")).sendKeys("scmuser");
		ff.findElement(By.id("loginButton")).click();
		
				
		

	}

}

