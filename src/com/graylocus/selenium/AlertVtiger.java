package com.graylocus.selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AlertVtiger {

public static void main(String[] args) {
		
		FirefoxDriver ff = new FirefoxDriver();
		
		ff.get("http://encreo.com/crm");
		ff.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		ff.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
		ff.manage().window().maximize();
		ff.findElement(By.name("username")).sendKeys("crmuser");
		ff.findElement(By.name("password")).sendKeys("crmuser");
		ff.findElement(By.xpath("//button[.='Sign in']")).click();
		
		ff.findElement(By.xpath("//strong[.='Leads']")).click();
		ff.findElement(By.xpath("//strong[.='Add Lead']")).click();
		
		ff.findElement(By.name("lastname")).sendKeys("some last name");
		ff.findElement(By.linkText("Cancel")).click();
		
		Alert leaveOrStay = ff.switchTo().alert();
		String message = leaveOrStay.getText();
		System.out.println("Message from the popup ::" + message);
		
		//leaveOrStay.accept();  // the default button in focus will be clicked
		leaveOrStay.dismiss();  // the other button will be clicked, if available or closes the dialog
	}


}

