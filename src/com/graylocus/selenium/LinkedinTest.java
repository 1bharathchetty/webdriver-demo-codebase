package com.graylocus.selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class LinkedinTest {
	static String linkedinURL = "https://www.linkedin.com/";
	static String firstName = "bharath";
	static String lastName ="chetty";
	static String emailID = "abcsafdsafdasfas234234234322342342342342432432423423@gmail.com";
	static String password = "thisiswrong";
	
	static String css_firstNameTextbox =  "input[id='first-name']";
	static String css_lastNameTextbox ="input[id='last-name']";
	static String css_emailTextbox ="input[name='emailAddress']";
	static String xpath_passwordTextbox = "//input[@name='password'][@type='password']";
	static String xpath_joinNowButton  = "//button[.='Join now']";
	static String xpath_errorMessage  = "//*[.='Someone’s already using that email.']";
	static String xpath_commonErrorMessage = "//*[contains(text(),'Someone’s already using that email')]";

	public static void main(String[] args) {

/*		startBrowser();
		goToLinkedIn();
		signUp();*/
		
		// starting the firefox browser
		//firefox is usually launched, with no cookies added
		// and no add-ons
		WebDriver firefox = new FirefoxDriver();

		
		//maximizing the firefox / browser window
		firefox.manage().window().maximize();
		
		//launching a particular URL
		//get waits until the page load is complete.
		firefox.get(linkedinURL);
		
		//setting timeouts for element not found cases
		firefox.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//setting timeouts for page load 
		firefox.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		
		
		WebElement firstnameTextbox = firefox.findElement(By.cssSelector(css_firstNameTextbox));
		firstnameTextbox.sendKeys(firstName);
		
		WebElement lastnameTextbox = firefox.findElement(By.cssSelector(css_lastNameTextbox));
		lastnameTextbox.sendKeys(lastName);
		
		WebElement emailTextbox = firefox.findElement(By.cssSelector(css_emailTextbox));
		emailTextbox.sendKeys(emailID);
		
		WebElement passwordTextbox = firefox.findElement(By.xpath(xpath_passwordTextbox));
		passwordTextbox.sendKeys(password);
		
		WebElement joinNowButton = firefox.findElement(By.xpath(xpath_joinNowButton));
		joinNowButton.click();
		
/*		WebElement errorMessageText = firefox.findElement(By.xpath(xpath_commonErrorMessage));
		
		boolean isErrorDisplayed = errorMessageText.isDisplayed();
		if(isErrorDisplayed==true){
			System.out.println("Registration not successful, email id is already in use");
		}else{
			System.out.println("No error message about email id");
		}*/
		
		List<WebElement> errorMessageElements = firefox.findElements(By.xpath(xpath_commonErrorMessage));
		if(errorMessageElements.size()>=1){
			System.out.println("Registration not successful, email id is already in use");
		}else{
			System.out.println("No error message about email id");
		}
		
	
	}

}

