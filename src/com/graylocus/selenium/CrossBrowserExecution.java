package com.graylocus.selenium;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class CrossBrowserExecution {

	/*	@Before
	public void runThisBeforeEveryTest(){
		System.out.println("running some initial setup before every test");
	}*/
	
	@Test
	public void usingFirefox(){
		System.out.println("running firefox browser");
		FirefoxDriver ff = new FirefoxDriver();
		ff.get("http://google.com");
		ff.manage().window().maximize();
		System.out.println("=>" + ff.getCurrentUrl());
	}

	@Test
	public void usingIE(){
		System.out.println("running IE browser");
		System.setProperty("webdriver.ie.driver", "D:\\bc\\ws\\WD\\drivers\\IEDriverServer64.exe");
		InternetExplorerDriver ie = new InternetExplorerDriver();
		ie.get("http://graylocus.com");
		ie.manage().window().maximize();		
		System.out.println("=>" + ie.getCurrentUrl());

	}
	
	@Test
	public void usingGoogleChrome(){
		System.out.println("running Google Chrome browser");
		System.setProperty("webdriver.chrome.driver", "D:\\bc\\ws\\WD\\drivers\\chromedriver.exe");
		ChromeDriver gc = new ChromeDriver();
		gc.get("http://encreo.com");
		gc.manage().window().maximize();
		System.out.println("=>" + gc.getCurrentUrl());


	}
	
/*	@After
	public void runThisAfterEveryTest(){
		System.out.println("running some initial setup after every test");

	}*/
}
