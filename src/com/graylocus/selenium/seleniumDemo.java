package com.graylocus.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class seleniumDemo {
	public static void main(String[] args){
		
		//chrome driver
		System.setProperty("webdriver.chrome.driver", "lib/chromedriver");
		WebDriver googleChrome = new ChromeDriver();
	
		//firefox driver
		System.setProperty("webdriver.gecko.driver", "lib/geckodriver");
		WebDriver firefox = new FirefoxDriver();
		
		googleChrome.manage().window().maximize();
		googleChrome.get("http://google.com");
		
		
		firefox.manage().window().maximize();
		firefox.get("http://linkedin.com");
		
		

	}
}
