package com.graylocus.selenium;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class AlertExamples {
	
	@Test
	public void alertFromSampleHTMLFile(){
		FirefoxDriver ff = new FirefoxDriver();
		ff.get("file:///D:/bc/ws/WD/sampleHTML/Popup.html");
		ff.findElement(By.xpath("//button[.='Try it']")).click();
		
		System.out.println("page title ::" +ff.getTitle());
		
	}
	
	@Test
	public void confirmDialogFromSampleHTMLFile(){
		FirefoxDriver ff = new FirefoxDriver();
		ff.get("file:///D:/bc/ws/WD/sampleHTML/AlertWithTwoButtons.html");
		ff.findElement(By.xpath("//button[.='Try it']")).click();
		Alert sampleAlert = ff.switchTo().alert();
		sampleAlert.accept(); // Clicks OK button or YES button
		sampleAlert.dismiss(); // Clicks Cancel button or No button
		//sampleAlert.authenticateUsing(creds);
		sampleAlert.getText();
		System.out.println("page title ::" +ff.getTitle());
		
	}

}

