package com.graylocus.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;


public class MyFirstTest {

	int count;
	String name = "bharath";
	static WebDriver driver;
	
	public static void main(String[] args) {
		
		System.out.println("My First Test");
		
		launchFirefox();
		goToVtigerLoginPage();
		logintoVtiger();
		goToLeadsPage();
		goToCreateLeadPage();
		createLead();
		signout();
		closeBrowser();
		
		
	}


	private static void launchFirefox() {
		System.out.println("launchFirefox");
		driver = new FirefoxDriver();  // This line helps you launch Firefox browser, usually minimized
		driver.manage().window().maximize(); // Helps you maximize the browser window
/*		driver = new ChromeDriver();
		driver = new SafariDriver();
		driver = new InternetExplorerDriver();*/
		
		
	}
	
	private static void goToVtigerLoginPage() {
		System.out.println("goToVtigerLoginPage");
		driver.get("http://encreo.com/crm"); // use this to launch your URL

	}
	
	private static void logintoVtiger() {
		System.out.println("logintoVtiger");
		WebElement usernameTextbox = driver.findElement(By.id("username"));
		//WebElement usernameTextbox = driver.findElement(By.xpath("//input[@id='username']"));
		usernameTextbox.sendKeys("crmuser");
		WebElement passwordTextbox = driver.findElement(By.name("password"));
		//WebElement passwordTextbox = driver.findElement(By.xpath("//input[@name='password']"));
		passwordTextbox.sendKeys("crmuser");
		WebElement signinButton = driver.findElement(By.tagName("button"));
		//WebElement signinButton = driver.findElement(By.xpath("//button"));
		signinButton.click();
	
	}


	
	private static void goToLeadsPage(){
		System.out.println("goToLeadsPage");
		WebElement leadsTab = driver.findElement(By.xpath("//strong[text()='Leads']"));
		leadsTab.click();

	}
	
	private static void goToCreateLeadPage(){
		System.out.println("goToCreateLeadPage");
		WebElement createLeadButton = driver.findElement(By.xpath("//strong[text()='Add Lead']"));
		createLeadButton.click();

	}
	
	
	private static void createLead(){
		System.out.println("createLead");
		WebElement firstNameTextbox = driver.findElement(By.xpath("//input[@name='firstname']"));
		firstNameTextbox.sendKeys("ram");
		WebElement lastNameTextbox = driver.findElement(By.xpath("//input[@id='Leads_editView_fieldName_lastname']"));
		lastNameTextbox.sendKeys("chetty");
		//WebElement firstSaveButton = driver.findElement(By.xpath("//strong[text()='Save']"));
		WebElement secondSaveButton = driver.findElement(By.xpath("(//strong[text()='Save'])[2]"));
		secondSaveButton.click();

	}
	
	private static void signout() {
		System.out.println("signout");
		WebElement usermenuLink = driver.findElement(By.xpath("//strong[text()='Demo']"));
		usermenuLink.click();
		WebElement signoutLink = driver.findElement(By.xpath("//a[text()='Sign Out']"));
		signoutLink.click();

	}
	
	private static void closeBrowser(){
		System.out.println("closeBrowser");
		driver.close();

	}

}

