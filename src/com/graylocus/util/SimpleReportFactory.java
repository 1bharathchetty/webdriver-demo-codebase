package com.graylocus.util;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;

public class SimpleReportFactory {

	private static ExtentReports reporter;

	public static synchronized ExtentReports getReporter() {
		if (reporter == null) {
			reporter = new ExtentReports("reports/SimpleReport1.html", true);
			
			reporter.config().documentTitle("Reports for login").reportHeadline("sai").reportName("regression");
			
		}
		return reporter;
	}

	public static synchronized void closeReporter() {
		reporter.flush();
		reporter.close();
	}

}
